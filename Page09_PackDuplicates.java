package com.tgl.homework100;

public class Page09_PackDuplicates {

	public static void main(String[] args) {
		char [] letters = {'a','a','a','a','b','c','c','a','a','d','e','e','e','e','d', 'f','f'};
		String s = new String(letters);
		int cnt = 0;
		for(int i = 1; i < s.length(); i++) {
			if((i == s.length()-1) && (s.charAt(i-1) == s.charAt(i))){
				System.out.println(s.substring(cnt, i+1));
			}else if((i == s.length()-1) && (s.charAt(i-1) != s.charAt(i))){
				System.out.printf("%s,", s.substring(cnt, i));
				System.out.println(s.charAt(i));
			}else if((s.charAt(i-1) == s.charAt(i)) && (i != s.length()-1)) {
				continue;
			}else{
				System.out.printf("%s,", s.substring(cnt, i));
				cnt = i;
			}
		}
	}
}
