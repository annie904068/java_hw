package com.tgl.homework100;

public class page19_SubstitutionDecoder {

	public static void main(String[] args) {
		String s = "9 14,5,22,5,18 19,1,23 1 16,21,18,16,12,5 3,15,23";
		decode(s);
	}
	
	public static void decode(String s) {
		StringBuilder  sb = new StringBuilder ();
		int num;
		int cnt = 0;
		char c;
		for(int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			num = Character.getNumericValue(c);
			if(c == ' ') {
				sb.append((char)(cnt+96));
				sb.append(' ');
				cnt = 0;
			}else if(c == ',') {
				sb.append((char)(cnt+96));
				cnt = 0;
			}else {
				if(cnt > 0) cnt = cnt*10+num;
				else cnt = num;
			}
		}
		sb.append((char)(cnt+96));
		System.out.println(sb);
	}
}
