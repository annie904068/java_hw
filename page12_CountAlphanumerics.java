package com.tgl.homework100;

public class page12_CountAlphanumerics {

	public static void main(String[] args) {
		String s = "1984 by George Orwell.";
		int cnt = 0;
		int i;
		for(int num = 0; num < s.length(); num++) {
			i = (int)s.charAt(num);
			if((i >= 65 && i <= 90) || (i >= 97 && i <= 122) || (i >= 48 & i <= 57)) {
				cnt++;
			}
		}
		System.out.println(cnt);
	}
}