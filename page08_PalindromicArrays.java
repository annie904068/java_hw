package com.tgl.homework100;

public class page08_PalindromicArrays {

	public static void main(String[] args) {
		String [] palindromic = {"Sausage", "Eggs", "Beans", "Beans", "Eggs", "Sausage"};
		String [] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		int paL = palindromic.length;
		int breakL = breakfast.length;
		int cnt1 = 0;
		int cnt2 = 0;
		for(int i = 0; i < paL; i++) {
			if(palindromic[i] == palindromic[paL - 1]) {
				cnt1 += 1;
				paL --;
			}else {
				break;
			}
		}
		if(cnt1 == paL) {
			System.out.println("True");
		}else {
			System.out.println("False");
		}
		
		for(int j = 0; j < breakL; j++) {
			if(breakfast[j] == breakfast[breakL - 1]) {
				cnt2 += 1;
				breakL--;
			}else {
				break;
			}
		}
		if(cnt2 == breakL) {
			System.out.println("True");
		}else {
			System.out.println("False");
		}
	}

}
