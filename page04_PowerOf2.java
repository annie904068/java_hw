package com.tgl.homework100;

import java.util.Scanner;

public class page04_PowerOf2 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int num;
		int i = 1;
		System.out.println("enter an number");
		num = scanner.nextInt();
		while(i <= num) {
			System.out.println(Math.pow(2, i));
			i++;
		}
	}

}
