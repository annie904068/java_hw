package com.tgl.homework100;

public class page02_OddNumbers {

	public static void main(String[] args) {
		int i = 0;
		while(i < 20) {
			if(i % 2 == 1) {
				System.out.println(i);
			}
			i ++;
		}
	}

}
