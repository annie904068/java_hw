package com.tgl.homework100;

public class page13_ReverseString {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		for(int l = s.length() - 1; l >= 0; l--) {
			System.out.print(s.charAt(l));
		}
	}

}
