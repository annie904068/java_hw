package com.tgl.homework100;

public class page14_Palindromes {

	public static void main(String[] args) {
		String sentence = "I never saw a purple cow";
		String palindrome = "rotavator";
		System.out.println(isPalindrome(sentence));
		System.out.println(isPalindrome(palindrome));
	}
	
	public static boolean isPalindrome(String s) {
		int cnt = 0;
		int l = s.length();
		for(int i = 0; i < (s.length()); i++) {
			if(s.charAt(i) == s.charAt(l-1)) {
				cnt += 1;
				l--;
			}else {
				break;
			}
		}
		if(cnt == s.length()) {
			return true;
		}else {
			return false;
		}
	}
}
