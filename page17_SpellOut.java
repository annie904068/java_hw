package com.tgl.homework100;

public class page17_SpellOut {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		System.out.println(spellOut(s));
	}
	
	public static StringBuilder spellOut(String s) {
		StringBuilder  sb = new StringBuilder (s);
		for(int i = 0; i < sb.length()-1; i++) {
			if (Character.isLetter(sb.charAt(i)) && Character.isLetter(sb.charAt(i+1))) {
				sb.insert(i+1, '-');
				i++;
			}
		}
		return sb;
	}
}
