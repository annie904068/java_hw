package com.tgl.homework100;

public class page07_ReverseAnArray {

	public static void main(String[] args) {
		String [] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		int num = breakfast.length - 1;
		while(num >= 0) {
			System.out.println(breakfast[num]);
			num --;
		}
	}

}
