package com.tgl.homework100;

public class page16_RemoveVowels {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		System.out.println(star(s));
	}
	
	public static String star(String s) {
		s = s.replaceAll("[aeiou]", "*").replaceAll("[AEIOU]", "*");
		return s;
	}
}
