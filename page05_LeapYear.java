package com.tgl.homework100;

import java.util.Scanner;

public class page05_LeapYear {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int year;
		System.out.println("Input the year:");
		year = scanner.nextInt();
		if((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
			System.out.println("leap year");
		}else {
			System.out.println("not leap year");
		}
		
	}
}