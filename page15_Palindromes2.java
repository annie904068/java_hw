package com.tgl.homework100;

public class page15_Palindromes2 {

	public static void main(String[] args) {
		String sentence = "I never saw a purple cow";
		String p = "Rise to vote, Sir!";		
		System.out.println(isPalindrome(sentence));
		System.out.println(isPalindrome(p));
	}
	
	public static boolean isPalindrome(String s) {
		s = s.replaceAll("[^a-zA-Z]", "").toLowerCase();
		int c = 0;
		int l = s.length();
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == s.charAt(l-1)) {
				c++;
				l--;
			}
		}
		if(c == s.length()) {
			return true;
		}else {
			return false;
		}
	}

}
