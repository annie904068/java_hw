package com.tgl.homework100;

public class page03_SquareNumbers {

	public static void main(String[] args) {
		int i = 1;
		int cnt = 1;
		while(cnt < 100) {
			cnt = i * i;
			System.out.println(cnt);
			i++;
		}
	}

}
