package com.tgl.homework100;

public class page11_CountTheLetter {

	public static void main(String[] args) {
		String s = "I never saw a purple cow";
		int cnt = 0;
		for(int num = 0; num < s.length(); num++) {
			if(s.charAt(num) == 'e') {
				cnt++;
			}
		}
		System.out.println(cnt);
	}

}
