package com.tgl.homework100;

public class page18_SubstitutionCipher {

	public static void main(String[] args) {
		String s = "Hello World";
		encode(s);
	}
	
	public static void encode(String s) {
		s = s.toLowerCase();
		StringBuilder  sb = new StringBuilder ();
		int l = s.length();
		int num;
		for(int i = 0; i < l; i++) {
			num = (int)s.charAt(i);
			if(num == 32) {
				sb.append(' ');
				sb.delete((sb.length()-2), (sb.length()-1));
			}
			if(num >= 97 && num <= 122) {
				sb.append(num-96);
				sb.append(',');
			}
		}
		sb.delete((sb.length()-1), (sb.length()));
		System.out.println(sb);
	}
}
