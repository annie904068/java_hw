package com.tgl.homework100;

public class page06_LastElementOfArray {

	public static void main(String[] args) {
		String [] breakfast = {"Sausage", "Eggs", "Beans", "Bacon", "Tomatoes", "Mushrooms"};
		System.out.println(breakfast[breakfast.length - 1]);
	}
}
